<?php
session_start();
echo '<nav class="navbar navbar-expand-lg container-fluid shadow bg-dark navbar-dark">
<div class="container-fluid">
    <a class="navbar-brand fw-bold" href="/forum">iDiscuss</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/forum">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    Catagories
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-disabled="true" href="contact.php">Contact</a>
            </li>
        </ul>';
        if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
            echo ' <form class="d-flex" role="search">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-success" type="submit">Search</button>
                <spanp class="text-light mx-2 my-2">welcome:'.$_SESSION['name'].'</spanp>
            </form>
            <button class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#logoutModal">Logout</button>';
        }
        else{
            echo '<form class="d-flex" role="search">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-success" type="submit">Search</button>
            </form>
            <div class="mx-2">
                <a class="text-decoration-none"  href="/forum/login.php"><button class="btn btn-outline-success">Login</button></a>
                <a class="text-decoration-none"  href="/forum/signup.php"><button class="btn btn-outline-success">Signup</button></a>
            </div>';
        }
   echo '</div>
</div>
</nav>';
// data-bs-toggle="modal" data-bs-target="#logoutModal"
// <button class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#signupModal"><a class="text-decoration-none text-light" href="/forum/logout.php">Logout</a></button>  
// include_once("./partial/_loginModel.php");
include_once("./partial/_logoutModel.php");

// include_once("./partial/_handleSignup.php");
// if(isset($_GET['signupsuccess']) && $_GET['signupsuccess']=="true"){
//         echo'<div class="alert alert-warning alert-dismissible fade show" role="alert">
//         <strong>signup successful!</strong> Now you can login.
//         <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
//     </div>';
// }

?>