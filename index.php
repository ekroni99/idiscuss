<?php
// session_start();
// if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
//     header("location: login.php");
//     exit();
// }
// else{
//     session_start();
// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to iDiscuse - Coding Forums</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>

<body>
    <?php include_once("./partial/_dbcon.php");?>
    <?php include_once("./partial/_header.php");?>
    <?php include_once("./partial/_carosul.php");?>
    <div class="container my-2">
        <h1 class="text-center">iDiscuss - Browse Catagories</h1>
        <div class="row my-3">
            <?php
                $sql_command="SELECT * FROM `categories`";
                $result=mysqli_query($con,$sql_command);
                while($row=mysqli_fetch_assoc($result)){
                    $category_id=$row['category_id'];
                    $category_name=$row['category_name'];
                    $category_description=$row['category_description'];
                    echo
                    '<div class="col-md-4">
                            <div class="card" style="width: 22rem;">
                                <img src="https://source.unsplash.com/500x300/?'.$category_name.',coding" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title"><a href="threadlist.php?catid='.$category_id.'">'.$row['category_name'].'</a></h5>
                                    <p class="card-text">'.substr($category_description,0,150).' .....</p>
                                    <a href="threadlist.php?catid='.$category_id.'" class="btn btn-primary">View Threads</a></a>
                                </div>
                            </div>
                    </div>';
                }
            ?>
        </div>
    </div>
    <?php include_once("./partial/_footer.php");?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
</body>

</html>