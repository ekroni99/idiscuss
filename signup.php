<?php
$showAlert=false;
$showError=false;
if($_SERVER['REQUEST_METHOD']=="POST"){
    include_once("./partial/_dbcon.php");
    $name=$_POST['signupName'];
    $email=$_POST['signupEmail'];
    $pass=$_POST['signupPassword'];
    $cpass=$_POST['signupConPass'];

    if(!isset($_POST['submit'])){
        if(!empty($name) && !empty($email) && !empty($pass) && !empty($cpass)){
            $uniquie_sqlCommand="SELECT * FROM `users` where user_name='$name' and user_email='$email'";
            $result=mysqli_query($con,$uniquie_sqlCommand);
            $numExitsRow=mysqli_num_rows($result);

            if($numExitsRow>0){
                $showError="UserName already Exits";
            }
            else{
                if($pass==$cpass){
                    $hash=password_hash($pass,PASSWORD_DEFAULT);
                    $insert_command="INSERT INTO `users` (`user_name`,`user_email`, `user_pass`, `timestamp`) VALUES ('$name','$email', '$hash', current_timestamp());";
                    $result=mysqli_query($con,$insert_command);
                    if($result){
                        $showAlert=true;
                    }
                    else{
                        $showError="There are some of problem, please try again Some time";
                    }
                }
                else{
                    $showError="Password did't match";
                }
            }
        }
        else{
            $showError="Some Input Field are Empty";  
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/_signumModel.css">
</head>

<body>
    <?php include_once("./partial/_header.php");?>
    <?php
    if($showAlert){
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Success!</strong> You account has been creates and now you can login.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }
    if($showError){
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Worning! </strong>'.$showError.'
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }
    ?>
    <section class="vh-100">
        <div class="container-fluid h-custom">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-md-9 col-lg-6 col-xl-5 mb-0 ms-4">
                    <img src="./images/draw2.webp" class="img-fluid" alt="Sample image">
                </div>
                <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-0 me-4">
                    <div class="container">
                        <h1 class="mb-2 text-success fw-bold">Welcome to idiscuss</h1>
                        <h3 class="mb-3 display-6 fs-4 text-secondary fw-bold">Signup an account</h3>
                    </div>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <div class="form-outline mb-3">
                            <input type="name" id="signupName" name="signupName" class="form-control form-control-lg"
                                placeholder="Enter a valid user name" />
                        </div>
                        <div class="form-outline mb-3">
                            <input type="email" id="signupEmail" name="signupEmail" class="form-control form-control-lg"
                                placeholder="Enter a valid email address" />
                        </div>
                        <div class="form-outline mb-3">
                            <input type="password" id="signupPassword" name="signupPassword"
                                class="form-control form-control-lg" placeholder="Enter password" />
                        </div>
                        <div class="form-outline mb-3">
                            <input type="password" id="signupConPassword" name="signupConPass"
                                class="form-control form-control-lg" placeholder="Enter confirm password" />
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg w-100"
                            style="padding-left: 2.5rem; padding-right: 2.5rem;">Signup</button>
                    </form>
                    <div class="text-center text-lg-start pt-2">
                        <p class="small fw-bold mt-2 pt-1 mb-0">already have an account? <a href="/forum/login.php"
                                class="link-danger">Login</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-white text-center mb-3 mb-md-0 py-4 px-4 px-xl-5 mt-1 bg-primary">
            Copyright ©ekRoni 2023. All rights reserved.
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
</body>

</html>