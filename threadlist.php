<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Welcome to iDiscuse - Coding Forums</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>

<body>
    <?php 
    include_once("./partial/_header.php");?>
    <?php include_once("./partial/_dbcon.php");?>
    <?php
        $id=$_GET['catid'];
        $sql_command="SELECT * FROM `categories` where category_id=$id";
        $result=mysqli_query($con,$sql_command);
        while($row=mysqli_fetch_assoc($result)){
            $category_id=$row['category_id'];
            $category_name=$row['category_name'];
            $category_description=$row['category_description'];
        }
    ?>
    <?php
    $showAlert=false;
    $method=$_SERVER['REQUEST_METHOD'];
    if($method=='POST'){
        $title=$_POST['title'];
        $desc=$_POST['desc'];
        $sno=$_POST['sno'];
        $insertSql_command="INSERT INTO `threads` (`thread_title`, `thread_desc`, `thread_cat_id`, `thread_user_id`, `timestamp`) VALUES ('$title', '$desc', '$id', '$sno', current_timestamp());";
        $result=mysqli_query($con,$insertSql_command);
        $showAlert=true;
        if($showAlert){
           echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Successful!</strong> Your thread has been added! Please for community to respond
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
        } else{
            echo '<div class="alert alert-warnign alert-dismissible fade show" role="alert">
                    <strong>warnig!</strong> Ops! There are some Problem..try again
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>';
        }

    }
   
    ?>
    <div class="container my-3">
        <div class="jumbotron bg-body-secondary text-black rounded p-4">
            <h1 class="display-4">Welcome to <?php echo $category_name;?> forums</h1>
            <p class="lead"><?php echo $category_description;?>.</p>
            <hr class="my-4">
            <p>This is a peer to peer forum for sharing knowledge with each other. Create unique posts. ...
                Keep posts courteous. ...
                Use respectful language when posting. ...
                Posting content from private messages and displaying that subject matter on the public forum is
                prohibited. ...
                Edit and delete posts as necessary using the tools provided by the forum.</p>
            <a class="btn btn-success btn-lg" href="#" role="button">Learn more</a>
        </div>
    </div>
    <?php
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
    echo '<div class="container">
        <h1 class="py-2">Start a Discussions</h1>
        <form action="'.$_SERVER["REQUEST_URI"].'" method="post">
        <input type="hidden" name="sno" id="sno" value="'.$_SESSION["sno"].'">
            <div class="mb-3">
                <label for="title" class="form-label">Problem Title</label>
                <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Keep your title as short and crisp as possible</div>
            </div>
            <div class="form-group mb-3">
                <label for="elaborate" class="form-label">Elaborate Your Problem</label>
                <textarea class="form-control" id="desc" name="desc" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>';
    }
    else{
        echo '
        <div class="container">
            <h1 class="py-2">Start a Discussions</h1>
            <div class="lead">You are not logged in. please login to be able to start a Discussion.</div>
        </div>';
    }
    ?>
    <div class="container">
        <h1 class="py-1">Browse Questions</h1>
        <?php
            $id=$_GET['catid'];
            $sql_command="SELECT * FROM `threads` where thread_cat_id=$id";
            $result=mysqli_query($con,$sql_command);
            $noResult=true;
            while($row=mysqli_fetch_assoc($result)){
                $noResult=false;
                $thread_id=$row['thread_id'];
                $thread_title=$row['thread_title'];
                $thread_description=$row['thread_desc'];
                $timestamp=$row['timestamp'];
                $thread_user_id=$row['thread_user_id'];
                $usersql="SELECT user_name FROM `users` where sno='$thread_user_id'";
                $result2=mysqli_query($con,$usersql);
                $row2=mysqli_fetch_assoc($result2);
                $user=$row2['user_name'];
                echo'
                <div class="media d-flex my-3">
                <img class="mr-3" src="./images/user.png" width="50px" height="40" alt="Generic placeholder image">
                <div class="media-body ms-1">
                    <p class="fw-bold my-0">'.$user.' at '.$timestamp.' </p>
                    <h5 class="mt-0"><a class="text-dark" href="thread.php?threadid='.$thread_id.'">'.$thread_title.'</a></h5>
                    '.$thread_description.'
                </div>
            </div>';
            }
            if($noResult){
              echo '<div class="jumbotron jumbotron-fluid bg-body-secondary text-black rounded p-4">
                    <div class="container">
                        <h1 class="display-4">No Thread Found</h1>
                        <p class="lead">Be the first person to ask a question.</p>
                    </div>
                </div>';
            }
        ?>
    </div>
    <?php include_once("./partial/_footer.php");?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
</body>

</html>