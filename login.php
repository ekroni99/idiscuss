<!-- <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="loginModalLabel">Login to iDiscuss</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div> -->
<?php
$showAlert=false;
$showError=false;

if($_SERVER['REQUEST_METHOD']=="POST"){
    include_once("./partial/_dbcon.php");
    $loginEmail=$_POST['loginEmail'];
    $loginPass=$_POST['loginPass'];
    if((!empty($loginEmail)) && (!empty($loginPass))){
    $check_comand2="SELECT * FROM `users` where user_email='$loginEmail'";
    $result=mysqli_query($con,$check_comand2);
    
    $num=mysqli_num_rows($result);
    if($num==1){
        while($row=mysqli_fetch_assoc($result)){
            if(password_verify($loginPass,$row['user_pass'])){
                $showAlert=true;
                session_start();
                $_SESSION['sno']=$row['sno'];
                $_SESSION['name']=$row['user_name'];
                $_SESSION['loggedin']=true;
                $_SESSION['loginEmail']=$loginEmail;
                header("location: /forum/index.php");
            }
            else{
                $showError="email or Password did't match";
            } 
        }  
    }
    else{
        $showError="email or Password did't match";
    } 
    }
    else{
    $showError="some input field are empty";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/_loginModel.css">
</head>

<body>
    <?php include_once("./partial/_header.php");?>
    <?php
    if($showError){
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Worning! </strong>'.$showError.'
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>';
    }
    ?>
    <section class="vh-100">
        <div class="container-fluid h-custom">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-md-9 col-lg-6 col-xl-5 me-5">
                    <img src="./images/draw.svg" width="600" height="350" alt="Sample image">
                </div>
                <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-0 me-5">
                    <div class="mb-4">
                        <h1 class="fw-bold text-success mb-3">Welcome to idiscuss</h1>
                        <h3 class="display-6 text-secondary fs-4 fw-bold mb-1">Please Login to your account</h3>
                    </div>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <div class="form-outline mb-4">
                            <input type="email" id="loginEmail" name="loginEmail" class="form-control form-control-lg"
                                placeholder="Enter a email address" />
                        </div>
                        <div class="form-outline mb-3">
                            <input type="password" id="loginPass" name="loginPass" class="form-control form-control-lg"
                                placeholder="Enter password" />
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg w-100"
                            style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>
                    </form>
                    <div class="text-center text-lg-start mt-0 ">
                        <p class="small fw-bold mt-3 pt-1 mb-0">don't have an account? <a href="/forum/signup.php"
                                class="link-danger">Signup</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-white text-center mt-1 mb-4 mb-md-0 py-4 px-4 px-xl-5 bg-primary">
            Copyright ©ekRoni 2023. All rights reserved.
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
</body>
</html>